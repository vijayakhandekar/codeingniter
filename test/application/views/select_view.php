<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">
	</script>
	<script>
	$(document).ready(function() {
		 
    $('#cust').DataTable( {
		"pageLength": 5
	} );
	
    });
	</script>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	#cust{
		text-align:center;
		}
	</style>
</head>
<body>

<div id="container">
	<h1>Customer Details</h1>

	<div id="body">
	   <div id="filter">
	     <form method="POST" id="frmfilt" action="<?php echo base_url() . 'home/search'; ?>">
		  
		  <input type="text" name="name" id="name" placeholder="Enter Name"/>
		    
		  <input type="text" name="email" id="email" placeholder="Enter Email"/>
		  <select name="age" id="age">
		  <option value="">Select age</option>
		  <option value="1">less than 25 years</option>
		  <option value="2">greater than or equal to 25 years</option>
		  <input type="submit" name="submit" id="btnfilt"/>
		 </form></br>
	   </div>
	 
		<table id="cust" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
            <th>Customer Id</th>  
            <th>Customer Name</th>  
			<th>Customer Email</th>  
            <th>Customer Address</th>  
			<th>Customer Zipcode</th>  
            <th>Customer telephone</th>  
			<th>Customer Date Of Birth</th>  
            </tr>
        </thead>
        
        <tbody>
            
           <?php 
		   
		    
           foreach ($all->result() as $row)  
           {
			?><tr>  
            <td><?php echo $row->c_id;?></td>  
            <td><?php  echo $row->c_name;?></td>  
			<td><?php  echo $row->c_email;?></td>
			<td><?php  echo $row->c_address;?></td>
			<td><?php  echo $row->c_zip;?></td>
			<td><?php  echo $row->c_telephone;?></td>
			<td><?php  echo $row->c_dob;?></td>
			</tr>
			<?php }   
           ?>  
            
           
            
        </tbody>
    </table>
		
	</div>

	
</div>

</body>
</html>