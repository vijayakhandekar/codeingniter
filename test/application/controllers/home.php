<?php
defined('BASEPATH') OR exit('No direct script access allowed');

   class home extends CI_Controller  
   {  
      public function index()  
      {  
         //load the database  
         $this->load->database();  
         	
		 //load the model  
         $this->load->model('select');  
         //load the method of model  
         $data['all']=$this->select->select();  
         //return the data in view  
         $this->load->view('select_view', $data);  
		 
      }
       public function search()  
      {
	   $data['name'] = $this->input->post('name');
	    $data['email'] = $this->input->post('email');
		 $data['age'] = $this->input->post('age');
       $data['submit'] = $this->input->post('submit');
		 
		if (isset($data) && !empty($data)) {
			
			//load the model  
         $this->load->model('select');  
		 
         //load the method of model  
         $data['all']=$this->select->search($data); 
        		 
         //return the data in view  
         $this->load->view('select_view', $data);  
		 
		}
	  } 
	  
   }  
?>  
